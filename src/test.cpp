#include <iostream>

#include "yunit/yunit.hpp"

int main()
{
    yunit::viewport view(0.0f, 100.0f, 0.0f, 100.0f);
    yunit::rect* one = view.create_rect();
    yunit::rect* two = view.create_rect();

    two->connect_left(one->get_left_axis());

    one->get_left_axis()->translate(10.0f)->apply();
    two->get_left_axis()->translate(10.0f)->apply();
    two->get_right_axis()->translate(-20.0f)->apply();
    
    std::cout << one->right() << std::endl;
    std::cout << two->width() << std::endl;

    return 0;
}
