#include <vector>
#include <algorithm>
#include <iostream>

namespace yunit
{
    struct axis_value
    {
        axis_value* anchor = NULL;
    
        float value = 0.0f;
        float offset = 0.0f;
        float anchor_value = 0.0f;

        std::vector<axis_value*> hooked;

        axis_value* recompute()
        {
            value = anchor_value + offset;

            return this;
        }

        axis_value* set_anchor_and_offset(float a, float o)
        {
            anchor_value = a;
            offset = o;

            return recompute();
        }

        axis_value* translate(float o)
        {
            offset += o;

            return recompute();
        }

        axis_value* set_offset(float o)
        {
            offset = o;

            return recompute();
        }

        axis_value* set_anchor(float a)
        {
            anchor_value = a;

            return recompute();
        }

        axis_value* anchor_to(axis_value* a)
        {
            if (anchor)
            {
                auto axis_index = std::find(
                    anchor->hooked.begin(), anchor->hooked.end(), this
                );

                if (axis_index != anchor->hooked.end())
                    anchor->hooked.erase(axis_index);
            }

            anchor = a;
            a->hooked.push_back(this);
            set_anchor(a->value);

            return propogate_changes();
        }

        axis_value* propogate_changes()
        {
            for (auto hook : hooked)
            {
                hook->set_anchor(value);
                hook->propogate_changes();
            }

            return this;
        }

        axis_value* apply()
        {
            recompute();

            return propogate_changes();
        }
    };

    struct point
    {
        axis_value* x_axis;
        axis_value* y_axis;

        float get_x()
        {
            return x_axis->value;
        }

        float get_y()
        {
            return y_axis->value;
        }

        point()
        {
            x_axis = new axis_value();
            y_axis = new axis_value();
        }
    };

    struct rect
    {
        point* min_point;
        point* max_point;

        float left()
        {
            return min_point->get_x();
        }

        float right()
        {
            return max_point->get_x();
        }

        float top()
        {
            return min_point->get_y();
        }

        float bottom()
        {
            return max_point->get_y();
        }

        float width()
        {
            return max_point->get_x() - min_point->get_x();
        }

        float height()
        {
            return max_point->get_y() - min_point->get_y();
        }

        float* grab_left()
        {
            return &(min_point->x_axis->offset);
        }

        float* grab_right()
        {
            return &(max_point->x_axis->offset);
        }

        float* grab_top()
        {
            return &(min_point->y_axis->offset);
        }

        float* grab_bottom()
        {
            return &(max_point->y_axis->offset);
        }

        axis_value* get_left_axis()
        {
            return min_point->x_axis;
        }

        axis_value* get_right_axis()
        {
            return max_point->x_axis;
        }

        axis_value* get_top_axis()
        {
            return min_point->y_axis;
        }

        axis_value* get_bot_axis()
        {
            return max_point->y_axis;
        }

        rect* connect_left(axis_value* a)
        {
            min_point->x_axis->anchor_to(a);
            return this;
        }

        rect* connect_right(axis_value* a)
        {
            max_point->x_axis->anchor_to(a);
            return this;
        }

        rect* connect_top(axis_value* a)
        {
            min_point->y_axis->anchor_to(a);
            return this;
        }

        rect* connect_bottom(axis_value* a)
        {
            max_point->y_axis->anchor_to(a);
            return this;
        }

        rect* apply_all(rect *rec)
        {
            rec->min_point->x_axis->recompute();
            rec->min_point->y_axis->recompute();
            rec->max_point->x_axis->recompute();
            rec->max_point->y_axis->recompute();
            rec->max_point->x_axis->propogate_changes();
            rec->max_point->y_axis->propogate_changes();
            return this;
        }

        rect* apply_left()
        {
            min_point->x_axis->recompute();
            min_point->x_axis->propogate_changes();
            return this;
        }

        rect* apply_right()
        {
            max_point->x_axis->recompute();
            max_point->x_axis->propogate_changes();
            return this;
        }

        rect* apply_top()
        {
            min_point->y_axis->recompute();
            min_point->y_axis->propogate_changes();
            return this;
        }

        rect* apply_bottom()
        {
            max_point->y_axis->recompute();
            max_point->y_axis->propogate_changes();
            return this;
        }

        rect()
        {
            min_point = new point();
            max_point = new point();
        }
    };

    struct viewport
    {
        float min_x;
        float max_x;
        float min_y;
        float max_y;

        viewport(float left, float right, float top, float bottom)
        {
            min_x = left;
            max_x = right;
            min_y = top;
            max_y = bottom;
        }

        /*
            ABOUT: Creates default rect. A default rect's top corner lays at viewport top corner,
            and the width and height are the width and height of the viewport. The bottom corner
            is anchored to the top, allowing for a set up where changing the x axis offset set the
            width and setting the y axis offset sets the height.
        */
        rect* create_rect()
        {
            rect* rec = new rect();

            rec->min_point->x_axis->set_anchor(min_x);
            rec->min_point->y_axis->set_anchor(min_y);

            rec->max_point->x_axis->set_offset(min_x + max_x);
            rec->max_point->y_axis->set_offset(min_y + max_y);

            rec->max_point->x_axis->anchor_to(rec->min_point->x_axis);
            rec->max_point->y_axis->anchor_to(rec->min_point->y_axis);

            return rec;
        }
    };
}
