# yunit

A simple and optimized layout engine. (Also header-only).

## What is it?
Yunit handles positioning and location of elements in a new and flexible way. It's still in it's early stages, but the api interface is relatively stable, only adding new helper functions.

## Contributing
Pull requests are very welcome! If you find a bug or have a feature you'd like resolved, please open an issue. We'll do our best to accomodate all feature requests, and squash all bugs. For all things that don't fall into the projects main goals, we'll hold a seperate branch for that "flavour" of yunit.

## Goals
We aim to make Yunit first and foremost fast, and secondly leightweight. Fast as in constructing `rectangles` as quickly as possible, and being able to move them around and adjust their properties as quickly as possible. By leightweight we mean keeping the library simple and memory footprint small. While keeping Yunit as fast as possible is the main priority, we do often tend to this goal as well.

## Usage
Usage information will be written at a later date, but for some examples check out `test.cpp` in the `src` directoy.